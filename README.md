## Prerequisites

You need to have [`minikube`](https://kubernetes.io/docs/tasks/tools/install-minikube/) and [`helm`](https://helm.sh/) installed and run the following commands:

- enable some minikube addons:

```
$ minikube addons enable ingress
$ minikube addons enable metrics-server
$ minikube addons enable heapster
```

- initialize `helm` (only for version 2):

```
$ helm init
```

- create the `lotto-app` namespace:

```
$ kubectl create namespace lotto-app
```

- add the helm repo:

```
$ helm repo add stable https://kubernetes-charts.storage.googleapis.com/
```

- install mongodb in the lotto-app namespace:

```
$ helm install mongodb stable/mongodb --namespace lotto-app
```

- Create the gitlab secret (only if using a private docker registry):

```
$ kubectl create secret docker-registry gitlab --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<password> --docker-email=<email> -n lotto-app
```

- Set the current docker registry url to point to minikube:

```
$ eval $(minikube docker-env)
```

- Build the docker images for each project:

```
$ docker build -t <user>/<project>:<tag> <folder>
```

### Kubernetes only

- Fixing tiller permissions:

```
$ kubectl --namespace kube-system create serviceaccount tiller

$ kubectl create clusterrolebinding tiller-cluster-rule \
 --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

$ kubectl --namespace kube-system patch deploy tiller-deploy \
 -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```

- install the nginx ingress:

```
$ helm install stable/nginx-ingress --name nginx-ingress --set controller.publishService.enabled=true
```

- Add the kubernetes dashboard:

```
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
$ kubectl apply -f dasboard.yaml
$ kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')
```
