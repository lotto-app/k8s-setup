#!/bin/bash

minikube addons enable ingress
minikube addons enable metrics-server
minikube addons enable heapster

helm init

kubectl create namespace lotto-app

echo -e '\n\n'
echo 'You still need to install mongodb and the nginx-ingress (using helm).'
echo "Also don't forget to add the gitlab secret."
echo -e 'Done.\n'